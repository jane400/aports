# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=glycin
pkgver=1.0.0
pkgrel=0
pkgdesc="Sandboxed and extendable image decoding"
url="https://gitlab.gnome.org/sophie-h/glycin"
# s390x: https://github.com/nix-rust/nix/issues/1968
arch="all !s390x"
license="MPL-2.0 OR LGPL-2.0-or-later"
makedepends="meson cargo clang16-dev gtk4.0-dev libheif-dev libjxl-dev libseccomp-dev"
source="https://gitlab.gnome.org/sophie-h/glycin/-/archive/$pkgver/glycin-$pkgver.tar.gz"
options="!check" # tests hang

build() {
	abuild-meson \
		-Dtest_skip_install=true \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
f947dd6ac5b75813e7ad6ec182ec927d91d28ce69f72373f63c2cee8426dc1aaccdc56379e02c839e08eab8d61394e40d823c28d0175c0cf7616f4ce26d9584b  glycin-1.0.0.tar.gz
"
